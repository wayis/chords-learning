export const chords = {
  Bb: {
    'Triade Majeure': 'Sib Ré Fa',
    'Triade Mineure': 'Sib Do# Fa'
  }
}