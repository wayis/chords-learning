import Head from 'next/head'
import Layout, {siteTitle} from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import {chords} from '../data/chords'

export default function Home() {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <p>Bienvenue sur l'apprentissage des accords</p>
        <p>
          (This is a sample website - you’ll be building a site like this on{' '}
          <a href="https://nextjs.org/learn">our Next.js tutorial</a>.)
        </p>
      </section>

      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Blog</h2>
        {chords.Bb['Triade Majeure']}
        <ul className={utilStyles.list}>
          {
            Object.keys(chords).map(note => (
                Object.keys(chords[note]).map(chordType=> (
                  <li className={utilStyles.listItem} key={{note}-{chordType}}>{chordType} de {note}: {chords[note][chordType]}</li>
                ))
            ))
          }
        </ul>
      </section>
    </Layout>
  )
}